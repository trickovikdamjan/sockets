package com.finki.sockets.configuration;

import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

public abstract class WebSocketAbstractConfig implements WebSocketMessageBrokerConfigurer {

    public static final String QUEUE = "/queue";
    public static final String TOPIC = "/topic";

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/stomp-endpoint")
                .setAllowedOrigins("*")
                .setHandshakeHandler(new HandshakeHandler())
                .withSockJS();
    }

}

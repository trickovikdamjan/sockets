package com.finki.sockets.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;

@Profile("cloud")
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketAlphaConfig extends WebSocketAbstractConfig {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app")
                .enableStompBrokerRelay(QUEUE, TOPIC)
                .setClientLogin("guest")
                .setClientPasscode("guest")
                .setRelayHost("rabbit");
    }


}

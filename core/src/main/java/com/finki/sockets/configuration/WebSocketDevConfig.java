package com.finki.sockets.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;

@Profile("dev")
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketDevConfig extends WebSocketAbstractConfig {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app")
                .enableStompBrokerRelay(QUEUE, TOPIC);

    }

}

package com.finki.sockets.controller;

import com.finki.sockets.domain.Greeting;
import com.finki.sockets.domain.Salute;
import com.finki.sockets.service.PushService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;

import java.security.Principal;

import static com.finki.sockets.configuration.WebSocketAbstractConfig.QUEUE;
import static com.finki.sockets.configuration.WebSocketAbstractConfig.TOPIC;

@Controller
public class GreetingController {
    private static final Logger logger = LoggerFactory.getLogger(GreetingController.class);

    private final PushService service;

    public GreetingController(PushService service) {
        this.service = service;
    }

    // User channel ???
    @MessageMapping(QUEUE)
    public void greeting(@Payload Salute message,
                         Principal principal) {
        final Greeting greeting = new Greeting(message.getName());
        logger.info("received message [{}] from [{}]", message, principal.getName());

        // Message has to be sent to /queue/{username}
        service.pushToPrivateSession(principal.getName(), greeting);
    }

    // General channel
    @MessageMapping(TOPIC + "/general")
    public void generalMessage(@Payload Salute message) {
        final Salute greeting = new Salute(message.getName());
        logger.info("received message [{}]", message);

        String path = TOPIC + "/general";
        service.broadcast(path, greeting);
    }


}

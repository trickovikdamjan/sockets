package com.finki.sockets.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class PushService {
    private static final Logger logger = LoggerFactory.getLogger(PushService.class);

    private final SimpMessagingTemplate template;

    public PushService(SimpMessagingTemplate template) {
        this.template = template;
    }

    public void pushToPrivateSession(String username, Object payload) {
        final String destination = String.format("/%s", username);
        logger.info("Message sent to [{}]", destination);
        template.convertAndSend(destination, payload);
    }

    public void broadcast(String topic, Object payload) {
        logger.info("Broadcasting message [{}] to topic [{}]", payload, topic);
        template.convertAndSend(topic, payload);
    }

}

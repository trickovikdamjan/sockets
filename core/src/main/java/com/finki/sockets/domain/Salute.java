package com.finki.sockets.domain;

public class Salute {

    private String name;

    public Salute() {

    }

    public Salute(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Salute{" +
                "name='" + name + '\'' +
                '}';
    }
}

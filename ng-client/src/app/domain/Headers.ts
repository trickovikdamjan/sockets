export interface Headers {
  destination: string,
  durable: boolean
  'auto-delete': boolean
}

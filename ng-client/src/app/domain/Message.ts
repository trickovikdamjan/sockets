export interface Message {
  destination: string,
  payload: any;
}

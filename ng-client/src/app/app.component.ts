import {Component, OnDestroy, OnInit} from '@angular/core';
import {queue, topic} from './config/socket-config';
import {SocketService} from './services/socket.service';
import {Salute} from './domain/Salute';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  public currentTopic: string;
  public subscriptions: Array<string>;
  public usernameTo: string;
  public name = 'general';
  public message: string;
  public salutes: string[] = [];

  public isConnected = false;

  constructor(private socketService: SocketService) {
  }

  ngOnInit() {
    this.subscriptions = new Array<string>();

    this.socketService.reFetchSubject.subscribe(_ =>
      this.subscriptions = this.socketService.getChannels
    );
  }

  ngOnDestroy(): void {
    this.socketService.disconnect();
  }

  setCurrentTopic(name: string) {
    this.currentTopic = name;
  }

  connect() {
    this.isConnected = true;
    const path: string = `${topic}/${this.currentTopic}`;
    this.socketService.connectToChannel<Salute>(path)
      .subscribe((it: Salute) => {
        this.salutes.push(it.name);
      }, err => {
        this.isConnected = false;
      });
  }

  disconnect() {
    this.socketService.disconnectFromChannel(this.currentTopic);
    if (this.subscriptions.length === 0) {
      this.isConnected = false;
    }
    this.currentTopic = null;
  }

  sendMessageToTopic() {
    const payload = {name: this.message};
    this.sendMessage(`${topic}/${this.currentTopic}`, payload);
  }

  sendMessage(destination: string, payload: any) {
    this.socketService.sendMessage(destination, payload);
  }



}

import {Injectable} from '@angular/core';

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {Observable, of, ReplaySubject, Subject} from 'rxjs';
import {bufferSize, socketURL, topic, windowTime} from '../config/socket-config';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private stompClient: Stomp = null;
  private channels: Map<string, ReplaySubject<any>> = new Map();
  readonly reFetchSubject: Subject<any> = new Subject();

  constructor() {
  }

  /**
   * T -> The expected message type
   * @param channelName -> uses the @socket.config predefined channels, it's up to the user of the function to pass them
   */
  connectToChannel<T>(channelName: string): Subject<T> {
    if (this.stompClient === null) {
      const sockJs = new SockJS(socketURL);
      this.stompClient = Stomp.over(sockJs);
      console.log('Connecting to the websocket');
    }

    if (this.channels.has(channelName)) {
      return this.channels.get(channelName);
    }

    const _that = this;
    const subject: ReplaySubject<T> = new ReplaySubject<T>(bufferSize, windowTime);



    this.stompClient.connect({}, (_) => {
      _that.stompClient.subscribe(channelName, (message) => {
        //Send the message to all the subscribers
        const body: T = JSON.parse(message.body);
        subject.next(body);
      });
    });

    this.channels.set(channelName, subject);
    this.reFetchSubject.next(null);
    console.log(this.channels);
    return subject;
  }

  /**
   * use only if it's sure that the channel exists
   * @param channelName
   */
  getChannel<T>(channelName: string): Subject<T> {
    return this.channels.get(channelName);
  }

  sendMessage(destination: string, payload: any) {
    const jsonPayload = JSON.stringify(payload);
    this.stompClient.send(destination, {}, jsonPayload);
  }

  disconnect() {
    this.channels.forEach(((value, _) => {
      value.complete();
      value.unsubscribe();
    }));
    this.channels.clear();
    this.stompClient.disconnect();
  }

  disconnectFromChannel(name: string) {
    const channel = this.getChannel(name);
    channel.complete();
    channel.unsubscribe();
    this.channels.delete(name);
    this.reFetchSubject.next(null);
    console.log("Deleting")
  }

  get getChannels(): string[] {
    const channels: string[] = [];
    this.channels.forEach(((_, key) => channels.push(key)));
    return channels;
  }

}

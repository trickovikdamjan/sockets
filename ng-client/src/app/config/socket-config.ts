export const socketURL: string = 'http://localhost:8762/core/stomp-endpoint';
export const queue: string = '/queue';
export const topic: string = '/topic';
export const message: string = '/app/msg';

export const notifications: string = '/notifications/general';

export const generalChannel: string = '/app/msg/general';
export const privateQueue: string = '/user/queue/private';

export const bufferSize: number = 20;
export const windowTime: number = 100;

# STOMP Endpint info
https://derickbailey.com/2015/07/22/airport-baggage-claims-selective-consumers-and-rabbitmq-anti-patterns/

The folowing endpoints need to be added to the app. For more info visit this [link](https://www.rabbitmq.com/stomp.html).
## Queue
For simple queues, destinations of the form <code>/queue/name</code> can be used.

Queue destinations deliver each message to at most <b>one</b> subscriber. Messages sent when no subscriber exists will be queued until a subscriber connects to the queue.

## Topic
Perhaps the most common destination type used by STOMP clients is <code>/topic/name</code>. They perform topic matching on publishing messages against subscriber patterns and can route a message to multiple subscribers (each gets its own copy). Topic destinations support all the routing patterns of [AMQP 0-9-1 topic exchanges](https://www.rabbitmq.com/tutorials/amqp-concepts.html).

Messages sent to a topic destination that has no active subscribers are simply discarded.

## Durable topics
The STOMP adapter supports durable topic subscriptions. Durable subscriptions allow clients to disconnect from and reconnect to the STOMP broker as needed, without missing messages that are sent to the topic.

Topics are neither durable nor transient, instead subscriptions are durable or transient. Durable and transient can be mixed against a given topic.

### Creating a durable topic
To create a <code>durable</code> subscription, set the <code>durable</code> header to <code>true</code> in the SUBSCRIBE frame. <code>persistent</code> is also supported as an alias for durable for backwards compatibility with earlier plugin versions.

When creating a durable subscription to a topic destination, set <code>auto-delete</code> to <code>false</code> to make sure the queue that backs your subscription is not deleted when last subscriber disconnects.

When creating a durable subscription, the <code>id</code> header must be specified. For example:

```
SUBSCRIBE
destination:/topic/my-durable
id:1234
durable:true
auto-delete:false
```

# Communication flow

## User to user
Every <code>/queue/\<something\></code> path can have only one subscriber.

A user can subscribe to <code>/queue/\<username\></code> to listen to all incoming messages for him.

A user can send messages to <code>/queue/\<username\></code> to another user with the predefined username <code>\<username\></code>.  

## Topic
Every <code>/topic/\<something\></code> path can have only one or more subscribers.

A user can subscribe to <code>/topic/\<name\></code> to listen to all incoming messages.

A user can send messages to <code>/topic/\<name\></code>, but if there are no subscribers the message is discarded.
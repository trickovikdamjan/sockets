#!/bin/zsh


echo "==========Creating all docker files============"
./gradlew createAllDockerfiles

echo "==========Building docker images=========="
sudo docker-compose -f docker-compose.yml build

echo "==========Starting all docker images=========="
sudo docker-compose -f docker-compose.yml up -d 
sudo docker-compose -f docker-compose.yml logs -f 



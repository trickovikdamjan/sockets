package com.finki.sockets.confgiration

import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class RouteConfiguration {

    @Bean
    fun routeLocator(builder: RouteLocatorBuilder): RouteLocator =
            builder.routes()
                    .route { predicateSpec ->
                        predicateSpec.path("/core/**")
                                .filters{ it.stripPrefix(1) }
                                .uri("lb://core/")
                    }.build()

}

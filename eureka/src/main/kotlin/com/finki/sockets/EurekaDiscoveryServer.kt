package com.finki.sockets

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

@SpringBootApplication
@EnableEurekaServer
class EurekaDiscoveryServer

fun main(args: Array<String>) {
    runApplication<EurekaDiscoveryServer>(*args)
}